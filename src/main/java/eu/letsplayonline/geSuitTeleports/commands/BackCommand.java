package eu.letsplayonline.geSuitTeleports.commands;

import eu.letsplayonline.geSuitTeleports.managers.TeleportsManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BackCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command,
                             String label, String[] args) {
        TeleportsManager.sendPlayerBack(sender);
        return true;
    }

}
